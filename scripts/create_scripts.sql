CREATE TABLE Sources (
    ID SERIAL PRIMARY KEY,
    Name TEXT NOT NULL,
    Type TEXT NOT NULL,
    Link TEXT,
    Category TEXT
);
CREATE TABLE News_Categories (
    ID SERIAL PRIMARY KEY,
    Name TEXT NOT NULL
);

CREATE TABLE News (
    ID SERIAL PRIMARY KEY,
    Source_ID INTEGER REFERENCES Sources(ID),
    Title TEXT NOT NULL,
    Text TEXT NOT NULL,
    Publication_Date TIMESTAMP NOT NULL,
    Category INTEGER REFERENCES News_Categories(ID) NOT NULL,
    Importance_Rating INTEGER
);


CREATE TABLE Users_Settings (
    ID SERIAL PRIMARY KEY,
    Username TEXT NOT NULL,
    Source_List INTEGER,
    Category_List INTEGER,
    Rating_Settings INTEGER
);

create TABLE user_sources (
    User_ID INTEGER REFERENCES Users_Settings(ID) NOT NULL,
    Source_ID INTEGER REFERENCES Sources(ID) NOT NULL
);

create TABLE user_categories (
    User_ID INTEGER REFERENCES Users_Settings(ID) NOT NULL,
    Category_ID INTEGER REFERENCES News_Categories(ID) NOT NULL
);

