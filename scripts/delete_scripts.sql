-- DELETE Queries:
--1. Удалить источник с ID 7:

DELETE FROM user_sources
WHERE Source_ID = 7;

UPDATE News
SET Source_ID = NULL
WHERE Source_ID = 7;

DELETE FROM Sources
WHERE ID = 7;

--2. Удалить новость с ID 5:

DELETE FROM News
WHERE ID = 5;

--3. Удалить настройки пользователя с ID 9:

DELETE FROM user_sources
WHERE User_ID = 9;

DELETE FROM user_categories
WHERE User_ID = 9;

DELETE FROM Users_Settings
WHERE ID = 9;

--4. Удалить связь пользователя с источником с ID 8:

DELETE FROM user_sources
WHERE Source_ID = 8;

--5. Удалить связь пользователя с категорией с ID 10:


DELETE FROM user_categories
WHERE Category_ID = 10;