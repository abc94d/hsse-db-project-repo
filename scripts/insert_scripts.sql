-- Populate Sources table
INSERT INTO Sources (Name, Type, Link, Category) VALUES
    ('BBC News', 'Online', 'https://www.bbc.com/news', 'General'),
    ('The New York Times', 'Online', 'https://www.nytimes.com/', 'General'),
    ('CNN', 'Online', 'https://www.cnn.com/', 'General'),
    ('TechCrunch', 'Online', 'https://techcrunch.com/', 'Technology'),
    ('National Geographic', 'Online', 'https://www.nationalgeographic.com/', 'Science'),
    ('ESPN', 'Online', 'https://www.espn.com/', 'Sports'),
    ('HuffPost', 'Online', 'https://www.huffpost.com/', 'General'),
    ('Scientific American', 'Online', 'https://www.scientificamerican.com/', 'Science'),
    ('The Guardian', 'Online', 'https://www.theguardian.com/', 'General'),
    ('Wired', 'Online', 'https://www.wired.com/', 'Technology');

-- Populate News_Categories table
INSERT INTO News_Categories (Name) VALUES
    ('Politics'),
    ('Business'),
    ('Technology'),
    ('Science'),
    ('Sports'),
    ('Health'),
    ('Entertainment'),
    ('Environment'),
    ('Education'),
    ('Travel');

-- Populate Users_Settings table
INSERT INTO Users_Settings (Username, Source_List, Category_List, Rating_Settings) VALUES
    ('user1', 1, 1, 1),
    ('user2', 2, 2, 2),
    ('user3', 3, 3, 3),
    ('user4', 4, 4, 4),
    ('user5', 5, 5, 5),
    ('user6', 6, 6, 6),
    ('user7', 7, 7, 7),
    ('user8', 8, 8, 8),
    ('user9', 9, 9, 9),
    ('user10', 10, 10, 10);

-- Populate user_sources table
INSERT INTO user_sources (User_ID, Source_ID) VALUES
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
    (10, 10);

-- Populate user_categories table
INSERT INTO user_categories (User_ID, Category_ID) VALUES
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
    (10, 10);
