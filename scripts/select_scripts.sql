-- SELECT Queries:

-- 2.1. Запросы с использованием WHERE, GROUP BY, HAVING:

--1. Выбрать количество новостей по каждой категории:

SELECT Category, COUNT(*) AS News_Count
FROM News
GROUP BY Category;

--2. Выбрать средний рейтинг важности новостей для каждого источника:

SELECT Source_ID, AVG(Importance_Rating) AS Avg_Rating
FROM News
GROUP BY Source_ID;

--3. Выбрать источники, у которых более 10 новостей:

SELECT Source_ID
FROM News
GROUP BY Source_ID
HAVING COUNT(*) > 10;

--4. Выбрать категории новостей, где количество новостей больше 100:

SELECT Category, COUNT(*) AS News_Count
FROM News
GROUP BY Category
HAVING COUNT(*) > 100;

--5. Выбрать источники, у которых нет ссылки:

SELECT ID, Name
FROM Sources
WHERE Link IS NULL;

-- 2.2. Запросы с использованием оконных функций:

--1. Получить ранжированные новости по дате публикации для каждой категории:

SELECT ID, Title, Category, Publication_Date,
       RANK() OVER (PARTITION BY Category ORDER BY Publication_Date DESC) AS Rank
FROM News;

--2. Выбрать новости и добавить столбец с общим количеством новостей для каждого источника:

SELECT ID, Title, Source_ID, 
       COUNT(*) OVER (PARTITION BY Source_ID) AS Total_News_Count
FROM News;

--3. Выбрать новости с относительной важностью (отношение рейтинга важности к максимальному рейтингу в группе):

SELECT ID, Title, Importance_Rating,
       Importance_Rating::FLOAT / MAX(Importance_Rating) OVER () AS Relative_Importance
FROM News;

--4. Выбрать пользователей и добавить столбец с рангом по их рейтингу:

SELECT ID, Username, Rating_Settings,
       RANK() OVER (ORDER BY Rating_Settings DESC) AS Rating_Rank
FROM Users_Settings;

--5. Выбрать новости и добавить столбец с суммарным рейтингом важности по каждой категории:

SELECT ID, Title, Category, Importance_Rating,
       SUM(Importance_Rating) OVER (PARTITION BY Category) AS Category_Importance_Sum
FROM News;
