-- UPDATE Queries:
--1. Обновить название источника с ID 1 на "BBC World News":

UPDATE Sources
SET Name = 'BBC World News'
WHERE ID = 1;

--2. Обновить тип источника с ID 4 на "Print":

UPDATE Sources
SET Type = 'Print'
WHERE ID = 4;

--3. Обновить дату публикации новости с ID 1 на текущую дату и время:

UPDATE News
SET Publication_Date = CURRENT_TIMESTAMP
WHERE ID = 1;

--4. Установить рейтинг важности новости с ID 2 в 8:

UPDATE News
SET Importance_Rating = 8
WHERE ID = 2;

--5. Обновить категорию пользователя с ID 3 на 5:

UPDATE Users_Settings
SET Category_List = 5
WHERE ID = 3;
